package com.example.newrecipeapplication.verification.presenter

interface VerificationImpl {

    interface ViewImpl{
        fun onNext(msg: String)
        fun showError(msg: String)
    }

}