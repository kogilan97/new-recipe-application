package com.example.newrecipeapplication.verification.view

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.newrecipeapplication.R
import com.example.newrecipeapplication.utils.Validations
import com.example.newrecipeapplication.verification.presenter.UserDBConnect
import com.example.newrecipeapplication.verification.presenter.VerificationImpl

class RegisterActivity:AppCompatActivity(),VerificationImpl.ViewImpl {

    private lateinit var tvLogin: TextView
    private lateinit var btnSignUp: Button
    private lateinit var etConfirmPassword: EditText
    private lateinit var etPassword:EditText
    private lateinit var etUsername: EditText
    lateinit var userDBConnect : UserDBConnect
    lateinit var validations: Validations

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        userDBConnect = UserDBConnect(this)
        validations = Validations()
        tvLogin = findViewById(R.id.tv_sign_in)
        btnSignUp = findViewById(R.id.btn_sign_up)
        etConfirmPassword =findViewById(R.id.et_confirm_password)
        etPassword =findViewById(R.id.et_password)
        etUsername=findViewById(R.id.et_username)
        tvLogin.setOnClickListener {
          finish()
        }
        btnSignUp.setOnClickListener {
        saveDetails(etUsername.text.toString(),etPassword.text.toString(),etConfirmPassword.text.toString())
        }

    }

    private fun saveDetails(username: String, password: String, confirmPassword: String) {

      if (! validations.emptyTextField(username) ||  !validations.emptyTextField(password) ||  !validations.emptyTextField(confirmPassword))
        {
            Toast.makeText(this,"Please provide all the requested information", Toast.LENGTH_LONG).show()
        }
        else if(!validations.comparePassword(password,confirmPassword))
        {
            Toast.makeText(this,"Password and Confirm Password are not same", Toast.LENGTH_LONG).show()
        }
        else
        {
            userDBConnect.onSaveDetails(username,password)
        }
    }

    override fun onNext(msg: String) {
        Toast.makeText(this,"User has been successfully registered.", Toast.LENGTH_LONG).show()
        finish()
    }

    override fun showError(msg: String) {
        Toast.makeText(this,msg, Toast.LENGTH_LONG).show()
        finish()
    }

}