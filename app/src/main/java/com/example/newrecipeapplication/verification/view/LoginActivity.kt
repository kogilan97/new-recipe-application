package com.example.newrecipeapplication.verification.view

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.newrecipeapplication.R
import com.example.newrecipeapplication.recipe.view.DashboardActivity
import com.example.newrecipeapplication.utils.Validations
import com.example.newrecipeapplication.verification.presenter.UserDBConnect
import com.example.newrecipeapplication.verification.presenter.VerificationImpl

class LoginActivity: AppCompatActivity(),VerificationImpl.ViewImpl {

    private lateinit var tvRegister: TextView
    private lateinit var etPassword: EditText
    private lateinit var etUsername: EditText
    private lateinit var btnLogin : Button
    lateinit var userDBConnect : UserDBConnect
    lateinit var validations: Validations

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        userDBConnect = UserDBConnect(this)
        validations = Validations()
        tvRegister = findViewById(R.id.tv_sign_up)
        etUsername=findViewById(R.id.et_username)
        etPassword=findViewById(R.id.et_password)
        btnLogin=findViewById(R.id.btn_login)
        tvRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
        btnLogin.setOnClickListener {
            onLogin(etUsername.text.toString(),etPassword.text.toString())
        }
    }
    private fun onLogin(username:String, password:String)
    {
        if (!validations.emptyTextField(username) ||  !validations.emptyTextField(password))
        {
            Toast.makeText(this,"Please provide all the requested information", Toast.LENGTH_LONG).show()
        }
        else
        {
            userDBConnect.onLoginDetails(username,password)
        }
    }

    override fun onNext(msg: String) {
        Toast.makeText(this,msg, Toast.LENGTH_LONG).show()
        startActivity(Intent(this, DashboardActivity::class.java))
        finish()
    }

    override fun showError(msg: String) {
        Toast.makeText(this,msg, Toast.LENGTH_LONG).show()
    }
}