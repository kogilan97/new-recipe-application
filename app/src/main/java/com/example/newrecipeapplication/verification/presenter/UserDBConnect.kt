package com.example.newrecipeapplication.verification.presenter

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.example.newrecipeapplication.database.DatabaseConfiguration
import com.example.newrecipeapplication.recipe.model.RecipeModel
import com.example.newrecipeapplication.verification.model.UserModel
import com.example.newrecipeapplication.verification.view.LoginActivity
import com.example.newrecipeapplication.verification.view.RegisterActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.QuerySnapshot
import java.util.ArrayList

class UserDBConnect {
     private lateinit var dbCollection:CollectionReference
     private lateinit var registerActivity: RegisterActivity
     private lateinit var loginActivity : LoginActivity
     private lateinit var dbConfiguration: DatabaseConfiguration
    private var flag:Boolean = false
     private val user = UserModel()


    constructor(view: RegisterActivity)
    {
        this.registerActivity = view
    }
     constructor(view: LoginActivity)
     {
         this.loginActivity = view
     }

    fun onSaveDetails(username:String, password:String)
    {
        user.username=username
        user.password=password
        dbConfiguration = DatabaseConfiguration()
        dbCollection= dbConfiguration.getCollection("users")
        dbCollection.add(user)
            .addOnSuccessListener {
                registerActivity.onNext("User has been successfully registered")
            }
            .addOnFailureListener { e ->
               registerActivity.onNext(e.toString())
            }
    }

     fun onLoginDetails(username:String,password:String)
     {
         dbConfiguration = DatabaseConfiguration()
         dbCollection=dbConfiguration.getCollection("users")

         dbCollection.get().addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
             if(task.isSuccessful){
                 for(document in task.result!!){
                     if(username == document.data["username"].toString() && password ==document.data["password"].toString())
                     {
                        flag=true
                         break
                     }
                 }
             }
             executeLogin()
         })

     }

    private fun executeLogin()
    {
        if(flag)
            loginActivity.onNext("User has successfully logged into the system")
        else
            Toast.makeText(this.loginActivity, "Wrong Username or Password", Toast.LENGTH_LONG).show()

    }

    }


