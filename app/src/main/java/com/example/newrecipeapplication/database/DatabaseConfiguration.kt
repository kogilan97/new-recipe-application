package com.example.newrecipeapplication.database

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore

class DatabaseConfiguration {
    private var fireBaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private var dbConfig: FirebaseFirestore = FirebaseFirestore.getInstance()
    private lateinit var dbUsers: CollectionReference


   fun getCollection(collectionName: String): CollectionReference {
       dbUsers = dbConfig.collection(collectionName)
       return dbUsers
   }

}