package com.example.newrecipeapplication

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.newrecipeapplication.recipe.view.DashboardActivity
import com.example.newrecipeapplication.verification.view.LoginActivity

class SplashActivity: AppCompatActivity() {
    private lateinit var ivLogo: ImageView
    private lateinit var tvLogo: TextView
    private val animTimeOut = 5000


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        ivLogo = findViewById(R.id.iv_app_logo)
        tvLogo = findViewById(R.id.tv_app_name)
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.anim_start)
        ivLogo.startAnimation(myAnim)
        tvLogo.startAnimation(myAnim)

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        Handler().postDelayed({ //  Log.d("tag2",EasyRideStorage.getInstance().getAuthToken());
                val i = Intent(this, LoginActivity::class.java)
                startActivity(i)
                finish()
        }, animTimeOut.toLong())

    }
}