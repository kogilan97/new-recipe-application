package com.example.newrecipeapplication.utils

import android.text.TextUtils
import android.widget.EditText
import java.time.temporal.IsoFields

class Validations {

    fun comparePassword(password: String, comparePassword: String) : Boolean
    {
        return password == comparePassword
    }

    fun emptyTextField(editTextValue: String) : Boolean
    {
        return editTextValue.isNotEmpty()
    }
}