package com.example.newrecipeapplication.recipe.presenter

import android.util.Log
import android.widget.Toast
import com.example.newrecipeapplication.database.DatabaseConfiguration
import com.example.newrecipeapplication.recipe.model.RecipeModel
import com.example.newrecipeapplication.recipe.view.DashboardActivity
import com.example.newrecipeapplication.recipe.view.ViewRecipeDialog
import com.example.newrecipeapplication.verification.model.UserModel
import com.google.firebase.database.DatabaseReference
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.Query
import java.util.ArrayList

class DashboardDBConnect {

    private lateinit var dbCollection: CollectionReference
    private var dashboardActivity: DashboardActivity
    private var viewRecipeDialog = ViewRecipeDialog()
    private lateinit var dbConfiguration: DatabaseConfiguration
    private var flag: Boolean = false
    private val recipeModel = RecipeModel()
    private val recipeData: ArrayList<RecipeModel> = ArrayList()
    private val singleRecipeData = RecipeModel()

    constructor(view: DashboardActivity) {
        this.dashboardActivity = view
    }

    fun onFetchRecipeDetails(recipeCategory: String) {
        recipeData.clear()
        dbConfiguration = DatabaseConfiguration()
        dbCollection = dbConfiguration.getCollection("recipe")
        dbCollection.whereEqualTo("category", recipeCategory)
            .get().addOnSuccessListener { it ->
                it.forEach {
                    var s = RecipeModel()
                    s.recipeId=it.id
                    s.category = it.get("category").toString()
                    s.name=it.get("name").toString()
                    s.image = it.get("image").toString()
                    s.rating = it.get("rating").toString()
                    recipeData.add(s)
                }
                dashboardActivity.onNext(recipeData)
            }
            .addOnFailureListener { e ->
                dashboardActivity.onError(e.toString())
            }


    }

    fun onViewDetailedRecipeDetails(recipeId:String, dashboardActivity: DashboardActivity)
    {
        dbConfiguration = DatabaseConfiguration()
        dbCollection = dbConfiguration.getCollection("recipe")
        dbCollection.document(recipeId).get()
            .addOnSuccessListener {
                var s = RecipeModel()
                s.recipeId=it.id
                s.category = it.get("category").toString()
                s.name=it.get("name").toString()
                s.image = it.get("image").toString()
                s.rating = it.get("rating").toString()
                s.steps=it.get("steps").toString()
                s.ingredients=it.get("ingredients").toString()
                viewRecipeDialog.onNextShowRecipe(s,dashboardActivity)
            }
            .addOnFailureListener { e ->
                viewRecipeDialog.onErrorShowRecipe(e.toString())
            }
    }

    fun onAddRecipeDetails(recipeCategory: String,recipeName: String,recipeRating: String, recipeSteps:String, recipeImage:String, recipeIngredients:String)
    {
        dbConfiguration = DatabaseConfiguration()
        dbCollection = dbConfiguration.getCollection("recipe")

        singleRecipeData.category = recipeCategory
        singleRecipeData.name = recipeName
        singleRecipeData.rating = recipeRating
        singleRecipeData.steps = recipeSteps
        singleRecipeData.image = recipeImage
        singleRecipeData.ingredients = recipeIngredients


        dbCollection.add(singleRecipeData)
            .addOnSuccessListener {
                dashboardActivity.onNextAddRecipe("Recipe has been successfully registered")
            }
            .addOnFailureListener { e ->
                dashboardActivity.onNextErrorRecipe(e.toString())
            }

    }

    fun onDeleteRecipeDetails(recipeId:String, dashboardActivity: DashboardActivity)
    {
        dbConfiguration = DatabaseConfiguration()
        dbCollection = dbConfiguration.getCollection("recipe")
        dbCollection.document(recipeId).delete()
            .addOnSuccessListener {
                dashboardActivity.onNextDeleteRecipe("Recipe has been successfully deleted")
            }
            .addOnFailureListener { e ->
                dashboardActivity.onErrorDeleteRecipe(e.toString())
            }
    }
}