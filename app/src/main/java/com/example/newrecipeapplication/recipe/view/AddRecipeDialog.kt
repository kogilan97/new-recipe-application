package com.example.newrecipeapplication.recipe.view

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.afollestad.materialdialogs.MaterialDialog
import com.bumptech.glide.Glide
import com.example.newrecipeapplication.R
import com.example.newrecipeapplication.recipe.presenter.DashboardDBConnect
import com.example.newrecipeapplication.utils.Validations
import java.sql.Struct
import kotlin.concurrent.thread

class AddRecipeDialog() : AppCompatActivity() {
    private lateinit var dashboardActivity: DashboardActivity
    private lateinit var radioGroup: RadioGroup
    private lateinit var etAddRecipeName: EditText
    private lateinit var etAddIngredients: EditText
    private lateinit var etAddSteps: EditText
    private lateinit var btnAddImage: Button
    private lateinit var btnAddRecipe: Button
    private lateinit var ivRecipeImage: ImageView
    private var mediaPath: String? = null
    private lateinit var imageView: ImageView
    private lateinit var dialog: Dialog
    private var postPath: String? = null
    private var validations = Validations()
    private lateinit var dashboardDBConnect: DashboardDBConnect

    fun showAddDialog(view: DashboardActivity) {
        dashboardDBConnect = DashboardDBConnect(view)
        this.dashboardActivity = view
        dialog = Dialog(view)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.view_add_recipe)

        radioGroup = dialog.findViewById(R.id.radio_add_category)
        etAddRecipeName = dialog.findViewById(R.id.et_add_name)
        etAddIngredients = dialog.findViewById(R.id.et_add_ingredients)
        etAddSteps = dialog.findViewById(R.id.et_add_steps)
        btnAddImage = dialog.findViewById(R.id.btn_add_image)
        btnAddRecipe = dialog.findViewById(R.id.btn_add)
        ivRecipeImage = dialog.findViewById(R.id.iv_add_image)
        btnAddImage.setOnClickListener(clickListner)


        Glide
            .with(dashboardActivity)
            .load(Uri.parse("https://www.nicepng.com/png/detail/214-2148603_you-eat-ready-to-eat-food-icon.png"))
            .thumbnail(0.1f)
            .into(ivRecipeImage)


        btnAddRecipe.setOnClickListener {

            val selectedId: Int = radioGroup.checkedRadioButtonId
            val radioButton: RadioButton = dialog.findViewById(selectedId)
            val selectedCategory = radioButton.text.toString()

            addRecipeDetails(
                selectedCategory,
                etAddIngredients.text.toString(),
                etAddSteps.text.toString(),
                etAddRecipeName.text.toString()
            )
        }
        dialog.show()
    }

    private val clickListner = View.OnClickListener { view ->

        if (ContextCompat.checkSelfPermission(
                dashboardActivity,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                dashboardActivity,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                0
            )
        } else {

            when (view.getId()) {
                R.id.btn_add_image -> MaterialDialog.Builder(this.dashboardActivity)
                    .title("Choose from gallery")
                    .items("Choose from gallery")
                    .itemsIds(R.array.itemIds)
                    .itemsCallback { dialog, view, which, text ->

                        when (which) {
                            0 -> {
                                val galleryIntent = Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                                )

                                dashboardActivity.startActivityForResult(galleryIntent,2)
                            }
                        }
                    }
                    .show()


            }


        }

    }

    private fun addRecipeDetails(
        category: String,
        ingredients: String,
        steps: String,
        name: String
    ) {
        if (!validations.emptyTextField(category) || !validations.emptyTextField(ingredients) || !validations.emptyTextField(
                steps
            ) || !validations.emptyTextField(name)
        ) {
            Toast.makeText(
                dashboardActivity,
                "Please provide all the requested information",
                Toast.LENGTH_LONG
            ).show()
        } else {
            val image: String =
                "https://www.nicepng.com/png/detail/214-2148603_you-eat-ready-to-eat-food-icon.png"
            val rating: String = "4/5"
            dashboardDBConnect.onAddRecipeDetails(
                category,
                name,
                rating,
                steps,
                image,
                ingredients
            )
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 2) {
                if (data != null) {
                    // Get the Image from data
                    val selectedImage = data.data
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                    val cursor =
                        contentResolver.query(selectedImage!!, filePathColumn, null, null, null)
                    assert(cursor != null)
                    cursor!!.moveToFirst()

                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    mediaPath = cursor.getString(columnIndex)
                    // Set the Image in ImageView for Previewing the Media

                    ivRecipeImage.setImageBitmap(BitmapFactory.decodeFile(mediaPath))
                    cursor.close()


                    postPath = mediaPath
                }

            }

        } else if (resultCode != Activity.RESULT_CANCELED) {
            Toast.makeText(this, "Sorry, there was an error!", Toast.LENGTH_LONG).show()
        }
    }

}


