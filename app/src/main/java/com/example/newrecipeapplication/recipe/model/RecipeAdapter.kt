package com.example.newrecipeapplication.recipe.model

import android.content.Context
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.bumptech.glide.Glide
import com.example.newrecipeapplication.R

class RecipeAdapter: BaseAdapter {
    private  var c:Context
   private  var recipeData: List<RecipeModel>
    private lateinit var view : View
    private lateinit var tvRecipeName : TextView
    private lateinit var tvRecipeRating : TextView
    private lateinit var ivRecipeImage : ImageView
    private lateinit var ivRecipeDelete : ImageView
    private lateinit var cvRecipe : CardView
    private lateinit var callback : RecipeCardViewClickListener
    constructor(c: Context,recipeData: List<RecipeModel> )
    {

        this.c = c;
        this.recipeData = recipeData;
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
       if(convertView==null)
       {
           view=LayoutInflater.from(c).inflate(R.layout.layout_cardview,parent,false)
       }
        tvRecipeName= view.findViewById(R.id.tv_recipe_name)
        tvRecipeRating= view.findViewById(R.id.tv_recipe_rating)
        ivRecipeImage= view.findViewById(R.id.iv_recipe_image)
        cvRecipe = view.findViewById(R.id.cv_Recipe)
        ivRecipeDelete = view.findViewById(R.id.iv_cancel)

        val s:RecipeModel = this.getItem(position) as RecipeModel
        tvRecipeName.text=s.name
        tvRecipeRating.text="Ratings: " +s.rating
       Glide
            .with(c)
            .load(Uri.parse(s.image))
            .into(ivRecipeImage)

        cvRecipe.setOnClickListener {
            callback.onItemClick(s.recipeId)
        }
        ivRecipeDelete.setOnClickListener {
            callback.onItemDeleteClick(s.recipeId)
        }


        return view
    }

    override fun getItem(position: Int): Any {
       return recipeData[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return recipeData.size
    }

    fun setOnItemClickListener(listener: RecipeCardViewClickListener) {
        callback = listener
    }

    interface RecipeCardViewClickListener {
        fun onItemClick(recipeID: String)
        fun onItemDeleteClick(recipeID: String)
    }
}