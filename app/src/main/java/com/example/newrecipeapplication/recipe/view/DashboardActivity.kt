package com.example.newrecipeapplication.recipe.view

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.afollestad.materialdialogs.MaterialDialog
import com.example.newrecipeapplication.R
import com.example.newrecipeapplication.recipe.model.RecipeAdapter
import com.example.newrecipeapplication.recipe.model.RecipeModel
import com.example.newrecipeapplication.recipe.presenter.DashboardDBConnect
import android.app.Application


class DashboardActivity : AppCompatActivity(),RecipeAdapter.RecipeCardViewClickListener {
    var adapter: RecipeAdapter? = null
    var gvRecipe: GridView? = null
    private lateinit var ivAddRecipe: ImageView
    lateinit var dashboardDBConnect: DashboardDBConnect
    private lateinit var btnAddImage : Button
    private var viewRecipeDialog = ViewRecipeDialog()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dasboard)
        dashboardDBConnect = DashboardDBConnect(this)
        val recipeCategory = resources.getStringArray(R.array.recipe_categories)
        ivAddRecipe=findViewById(R.id.iv_add_recipe)

        // access the spinner
        val spinner = findViewById<Spinner>(R.id.spRecipeCategory)
        if (spinner != null) {
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item, recipeCategory
            )
            spinner.adapter = adapter
        }
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                onShowDetails(parent?.getItemAtPosition(position).toString())
            }
        }
        gvRecipe = findViewById<View>(R.id.gv) as GridView

        ivAddRecipe.setOnClickListener {
             val addRecipeDialog = AddRecipeDialog()
            addRecipeDialog.showAddDialog(this)
        }
    }

    private fun onShowDetails(recipeCategory: String) {
        dashboardDBConnect.onFetchRecipeDetails(recipeCategory)
    }

    fun onNext(recipeData: List<RecipeModel>) {
        adapter = RecipeAdapter(this, recipeData)
        gvRecipe!!.adapter = adapter
        adapter!!.setOnItemClickListener(this)
    }

    fun onError(error: String) {
        Toast.makeText(this,error,Toast.LENGTH_LONG).show()
    }

    override fun onItemClick(recipeID: String) {
        viewRecipeDialog.showRecipeInfo(this,recipeID)
    }

    override fun onItemDeleteClick(recipeID: String) {
        dashboardDBConnect.onDeleteRecipeDetails(recipeID,this)
    }

    fun onNextAddRecipe(message: String)
    {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show()
        finish();
        startActivity(intent)
    }

    fun onNextErrorRecipe(error: String)
    {
        Toast.makeText(this,error,Toast.LENGTH_LONG).show()
    }

    fun onNextDeleteRecipe(message: String)
    {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show()
        finish();
        startActivity(intent)
    }

    fun onErrorDeleteRecipe(error: String)
    {
        Toast.makeText(this,error,Toast.LENGTH_LONG).show()
    }


}