package com.example.newrecipeapplication.recipe.model

class RecipeModel {
    var recipeId: String=""
    var category:String =""
    var name:String =""
    var rating: String = ""
    var ingredients: String =""
    var steps: String=""
    var image: String=""

}