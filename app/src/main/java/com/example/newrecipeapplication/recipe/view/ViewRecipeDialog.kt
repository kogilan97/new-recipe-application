package com.example.newrecipeapplication.recipe.view

import android.app.Dialog
import android.net.Uri
import android.util.Log
import android.view.Window
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.TransformationUtils.centerCrop
import com.example.newrecipeapplication.R
import com.example.newrecipeapplication.recipe.model.RecipeModel
import com.example.newrecipeapplication.recipe.presenter.DashboardDBConnect

class ViewRecipeDialog {
    private lateinit var dashboardDBConnect: DashboardDBConnect
    private lateinit var dashboardActivity : DashboardActivity
    private lateinit var  recipeId: String
    private lateinit var tvRecipeName: TextView
    private lateinit var tvRecipeCategory: TextView
    private lateinit var tvRecipeRating: TextView
    private lateinit var tvRecipeIngredients: TextView
    private lateinit var tvRecipeSteps: TextView
    private lateinit var ivRecipeImage: ImageView
    private lateinit var recipeData: RecipeModel
    private lateinit var dialog: Dialog

    fun showRecipeInfo(view:DashboardActivity,recipeId:String)
    {
        this.dashboardActivity= view
        this.recipeId=recipeId
        dashboardDBConnect = DashboardDBConnect(this.dashboardActivity)
        dashboardDBConnect.onViewDetailedRecipeDetails(this.recipeId,view)
    }

    fun onNextShowRecipe(recipeData: RecipeModel,dashboardActivity: DashboardActivity) {
        dialog = Dialog(dashboardActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.view_detailed_recipe)
        tvRecipeName = dialog.findViewById(R.id.tv_view_name)
        tvRecipeCategory = dialog.findViewById(R.id.tv_view_category)
        tvRecipeRating = dialog.findViewById(R.id.tv_view_rating)
        tvRecipeIngredients = dialog.findViewById(R.id.tv_view_ingredients)
        tvRecipeSteps = dialog.findViewById(R.id.tv_view_steps)
        ivRecipeImage = dialog.findViewById(R.id.iv_view_recipe)
        this.recipeData=recipeData

        tvRecipeName.text = recipeData.name
        tvRecipeCategory.text = recipeData.category
        tvRecipeRating.text = recipeData.rating
        tvRecipeIngredients.text = recipeData.ingredients
        tvRecipeSteps.text = recipeData.steps
        Glide
            .with(dashboardActivity)
            .load(Uri.parse(recipeData.image))
            .thumbnail(0.1f)
            .into(ivRecipeImage)
        dialog.show()
    }

    fun onErrorShowRecipe(e:String) {
        Toast.makeText(dashboardActivity,e, Toast.LENGTH_LONG).show()
    }
}